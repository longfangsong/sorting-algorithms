package murraco;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import murraco.BubbleSort;
import murraco.Heapsort;
import murraco.InsertionSort;
import murraco.MergeSort;
import murraco.Quicksort;
import murraco.SelectionSort;

interface IntegerSorter {
  public void sortIntegers(Integer[] data);
}

interface PairCompareByFirstSorter {
  public void sortByFirst(PairCompareByFirst[] data);
}

interface PairCompareBySecondSorter {
  public void sortBySecond(PairCompareBySecond[] data);
}

public class Group23Test {
  /**
   * Run tests on all sort algorithms
   *
   * @param data     integers for sorting
   * @param expected string form of the expected result
   */
  public void testForAllSortAlgorithms(Integer[] data, String expected) {
    IntegerSorter[] sorters = {
        (Integer[] d) -> BubbleSort.bubbleSort(d),
        (Integer[] d) -> Heapsort.heapSort(d),
        (Integer[] d) -> InsertionSort.insertionSort(d),
        (Integer[] d) -> MergeSort.mergeSort(d),
        (Integer[] d) -> Quicksort.quickSort(d),
        (Integer[] d) -> SelectionSort.selectionSort(d),
    };
    for (int i = 0; i < sorters.length; ++i) {
      Integer[] d = data.clone();
      sorters[i].sortIntegers(d);
      assertEquals(String.format("testForAllSortMethods failed on %d", i), expected,
          Arrays.toString(d));
    }
  }

  /**
   * Run tests on all stable sort algorithms, ie. bubble and insertion sort
   * First sorting with the first element of the pairs, then do the second
   *
   * @param data     pairs for sorting
   * @param expected string form of the expected result
   */
  public void testForStableSortAlgorithms(Pair[] data, String expected) {
    PairCompareByFirstSorter[] firstSorters = {
        (PairCompareByFirst[] d) -> BubbleSort.bubbleSort(d),
        (PairCompareByFirst[] d) -> InsertionSort.insertionSort(d),
    };
    PairCompareBySecondSorter[] secondSorters = {
        (PairCompareBySecond[] d) -> BubbleSort.bubbleSort(d),
        (PairCompareBySecond[] d) -> InsertionSort.insertionSort(d),
    };
    for (int i = 0; i < firstSorters.length; ++i) {
      // first sort the pairs by the first element
      PairCompareByFirst[] compareByFirst = new PairCompareByFirst[data.length];
      for (int j = 0; j < data.length; ++j) {
        compareByFirst[j] = new PairCompareByFirst(data[j]);
      }
      firstSorters[i].sortByFirst(compareByFirst);
      // then sort them by the second
      PairCompareBySecond[] compareBySecond = new PairCompareBySecond[data.length];
      for (int j = 0; j < data.length; ++j) {
        compareBySecond[j] = new PairCompareBySecond(compareByFirst[j].pair);
      }
      secondSorters[i].sortBySecond(compareBySecond);
      // Check the result
      assertEquals(String.format("testForStableSortMethods failed on %d", i),
          expected,
          Arrays.toString(compareBySecond));
    }
  }

  // kept for consistency with the original test
  @Test
  public void testOrigin() {
    final Integer[] data = { 4, 3, 0, 11, 7, 5, 15, 12, 99, 1 };
    testForAllSortAlgorithms(data, "[0, 1, 3, 4, 5, 7, 11, 12, 15, 99]");
  }

  @Test
  public void testEmpty() {
    final Integer[] data = {};
    testForAllSortAlgorithms(data, "[]");
  }

  @Test
  public void testOne() {
    final Integer[] data = { 1 };
    testForAllSortAlgorithms(data, "[1]");
  }

  @Test
  public void testTwoSame() {
    final Integer[] data = { 1, 1 };
    testForAllSortAlgorithms(data, "[1, 1]");
  }

  @Test
  public void testTwoDifferent() {
    final Integer[] data = { 2, 1 };
    testForAllSortAlgorithms(data, "[1, 2]");
  }

  @Test
  public void testNull() {
    Integer[] d = null;
    BubbleSort.bubbleSort(d);
    Heapsort.heapSort(d);
    InsertionSort.insertionSort(d);
    MergeSort.mergeSort(d);
    Quicksort.quickSort(d);
    SelectionSort.selectionSort(d);
  }

  @Test
  public void testAlreadySorted() {
    final Integer[] data = { 1, 2, 3 };
    testForAllSortAlgorithms(data, "[1, 2, 3]");
  }

  @Test
  public void testAlreadySortedReverse() {
    final Integer[] data = { 3, 2, 1 };
    testForAllSortAlgorithms(data, "[1, 2, 3]");
  }

  @Test
  public void testStable() {
    final Pair[] data = {
        new Pair(2, 2),
        new Pair(2, 1),
        new Pair(1, 1),
        new Pair(3, 1),
    };
    testForStableSortAlgorithms(data, "[(1, 1), (2, 1), (3, 1), (2, 2)]");
  }

  @Test
  public void testMergeSortIncToDec() { // L31
    final Integer[] data = { 3, 0, 2, 3, 4, 3, 2, 1 };
    MergeSort.mergeSort(data);
    assertEquals("[0, 1, 2, 2, 3, 3, 3, 4]", Arrays.toString(data));
  }

  @Test
  public void testHeapsortChangeBound() { // L20 and 23
    final Integer[] data = { 3, 1, 9, 3, 9, 8, 7, 6, 2, 4, 8 };
    Heapsort.heapSort(data);
    assertEquals("[1, 2, 3, 3, 4, 6, 7, 8, 8, 9, 9]", Arrays.toString(data));
  }
}
