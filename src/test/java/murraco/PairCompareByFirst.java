package murraco;

public class PairCompareByFirst implements Comparable<PairCompareByFirst> {
    public Pair pair;

    public PairCompareByFirst(Pair pair) {
        this.pair = new Pair(pair.first, pair.second);
    }

    @Override
    public int compareTo(PairCompareByFirst o) {
        return this.pair.first.compareTo(o.pair.first);
    }

    @Override
    public String toString() {
        return pair.toString();
    }
}
