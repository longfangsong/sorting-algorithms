package murraco;

public class PairCompareBySecond implements Comparable<PairCompareBySecond> {
    public Pair pair;

    public PairCompareBySecond(Pair pair) {
        this.pair = new Pair(pair.first, pair.second);
    }

    @Override
    public int compareTo(PairCompareBySecond o) {
        return this.pair.second.compareTo(o.pair.second);
    }

    @Override
    public String toString() {
        return pair.toString();
    }
}