package murraco;

public class Pair {
    public Pair(Integer first, Integer second) {
        this.first = first;
        this.second = second;
    }

    public Integer first;
    public Integer second;

    @Override
    public String toString() {
        return "(" + first + ", " + second + ")";
    }
}
